let input = document.createElement('input')
input.id = 'input'
input.style.width = '50px'
document.body.append(input)
input.insertAdjacentHTML('beforebegin', '<span>Price</span>')
input.onfocus = function () {
    input.style.borderColor = 'green'
}
input.addEventListener('blur', function () {
    let inputValue = +document.getElementById('input').value
    if (isValidValue(inputValue)) {
        validMessage(inputValue)
    } else {
        invalidMessage()
    }
})
function isValidValue(inputValue) {
    if (inputValue <= 0 || !inputValue) {
        return false
    } return true
}
function validMessage(inputValue) {
    let validMessage = document.querySelector('.validMessage')
    if(validMessage) {
        validWord.innerText = `Текущая цена: ${inputValue}`
        input.style.color = 'green'
        input.style.borderColor = '#000'
        input.style.outline = 'none'
        removeInvalidMessage()
    } else {
        div = document.createElement('div')
        div.id = 'div'
        validWord = document.createElement('span')
        validWord.classList.add('validMessage')
            validWord.innerText = `Текущая цена: ${inputValue}`
        document.body.prepend(div)
        validWord.classList.add('validMessage')
        let btn = document.createElement('button')
        btn.innerText = 'X'
        btn.style.cssText = `
            border-radius: 50%;
            `
        document.getElementById('div').append(btn)
        document.getElementById('div').prepend(validWord)
        btn.onclick = function removeDiv() {
            let elem = document.getElementById('div');
            elem.parentNode.removeChild(elem);
            input.value = ' '
        }
        input.style.color = 'green'
        input.style.borderColor = '#000'
        input.style.outline = 'none'
        removeInvalidMessage()
    }
    } 

function invalidMessage() {
let invalidMessage = document.querySelector('.invalidMessage')
if(invalidMessage) {
    input.style.borderColor = 'red'
    input.style.outline = 'none'
    input.value = ' '
}
else {
    let invalidWord = document.createElement('p')
    invalidWord.innerText = 'Please enter correct price'
    document.body.append(invalidWord)
    input.style.borderColor = 'red'
    input.style.outline = 'none'
    input.value = ' '
invalidWord.classList.add('invalidMessage')}

}
function removeInvalidMessage() {
    let invalidMessage = document.querySelector('.invalidMessage')
    if(invalidMessage) {
        invalidMessage.remove()
    }   
}

